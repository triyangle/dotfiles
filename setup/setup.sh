#!/usr/bin/env bash

source ~/dotfiles/setup/symlink.sh
source ~/dotfiles/setup/prezto/setup.sh

source ~/dotfiles/env/setup.sh

source ~/dotfiles/setup/vim.sh
source ~/dotfiles/setup/tmux.sh
source ~/dotfiles/setup/jupyter/setup.sh
